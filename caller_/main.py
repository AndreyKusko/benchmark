from itertools import count

from clients.orm import (
    DjangoORMPython,
    ORMRestAPI,
    PythonSqlAlchemyAsyncRepo,
    PythonSqlAlchemySyncRepo,
    PythonSqlAlchemyAsyncSession,
)
import sys

from clients.frameworks import RestAPI, FastApiPython, FastApiPypy, LitestarPython, DjangoRestPython, BlackSheepPython, \
    GRPC_Python, GRPC_Pypy, GRPS, AIOHTTPPython
import time
from prettytable import PrettyTable

POST = "POST"
GET = "GET"
PATCH = "PATCH"
PUT = "PUT"
DELETE = "DELETE"

TOTAL = "total secs"
# MEAN = "mean secs/request"
MEAN = "secs/request"
FRAMEWORK = "framework"
ENDPOINT = "endpoint"
METHOD = "method"
PARALLELISM = "parallelism"
ASYNC = "async"
SYNC = "sync"
COLUMNS = [ENDPOINT, FRAMEWORK, TOTAL, MEAN]
HTTP_COLUMNS = [METHOD, PARALLELISM, ENDPOINT, FRAMEWORK, TOTAL, MEAN]


def ask_orm(qty):
    print(f"make {qty} ORM requests")
    table = PrettyTable(HTTP_COLUMNS)
    frameworks = [
        DjangoORMPython,
        PythonSqlAlchemyAsyncRepo,
        PythonSqlAlchemySyncRepo,
        PythonSqlAlchemyAsyncSession,
    ]
    endpoints = [
        # (method, parallelism, e.__name__)
        (*additional, e.__name__)
        for *additional, e in [
            (POST, SYNC, ORMRestAPI.create),
            (GET, SYNC, ORMRestAPI.retrieve),
            (PUT, SYNC, ORMRestAPI.change),
            (GET, SYNC, ORMRestAPI.list),
            (DELETE, SYNC, ORMRestAPI.remove),
        ]
    ]

    results = list()
    for framework in frameworks:
        client = framework()
        # client.stop_server()
        # client.start_server()
        client.check_health()
        client.clean_db()
        for method, parallelism, e in endpoints:
            print('    ', method, parallelism, e)
            endpoint = getattr(client, e)
            total, mean = endpoint(qty)
            results.append(
                {
                    FRAMEWORK: framework.__name__,
                    METHOD: method,
                    PARALLELISM: parallelism,
                    ENDPOINT: e,
                    TOTAL: total,
                    MEAN: mean
                }
            )

        client.clean_db()
        # client.stop_server()

    results.sort(key=lambda e: e[MEAN])

    for *_, e in endpoints:
        for result in results:
            if result[ENDPOINT] != e:
                continue
            table.add_row([result[c] for c in HTTP_COLUMNS])
        table.add_row(["* * *" for _ in HTTP_COLUMNS])
    print(table)


def ask_http_requests(qty):
    print(f"make {qty} HTTP requests")
    table = PrettyTable(HTTP_COLUMNS)
    frameworks = [
        AIOHTTPPython,
        DjangoRestPython,
        BlackSheepPython,
        LitestarPython,
        FastApiPypy,
        FastApiPython,
    ]
    endpoints = [
        # (method,parallelism, e.__name__)
        (*additional, e.__name__)
        for *additional, e in [
            (GET, SYNC, RestAPI.dictionary,),
            (GET, SYNC, RestAPI.response__dict,),
            (GET, SYNC, RestAPI.serialization,),
            (POST, SYNC, RestAPI.deserialize_n_serialize),
        ]
    ]

    results = list()
    for framework in frameworks:
        client = framework()
        for method, parallelism, e in endpoints:
            print('    ', method, parallelism, e)
            endpoint = getattr(client, e)
            total, mean = endpoint(qty)
            results.append(
                {
                    FRAMEWORK: framework.__name__,
                    METHOD: method,
                    PARALLELISM: parallelism,
                    ENDPOINT: e,
                    TOTAL: total,
                    MEAN: mean
                }
            )

    results.sort(key=lambda e: e[MEAN])

    for *_, e in endpoints:
        for result in results:
            if result[ENDPOINT] != e:
                continue
            table.add_row([result[c] for c in HTTP_COLUMNS])
        table.add_row(["* * *" for _ in HTTP_COLUMNS])
    print(table)


def ask_grpc(qty):
    results = list()
    table = PrettyTable(COLUMNS)
    print(f'make {qty} GRPC requests')
    endpoints = [
        e.__name__
        for e in [
            GRPS.with_reopen_channel__string,
            GRPS.with_single_channel__string,
            GRPS.with_single_channel__dictionary,
        ]
    ]
    for framework in [
        GRPC_Python,
        GRPC_Pypy
    ]:
        client = framework()
        for e in endpoints:
            print('    ', e)
            endpoint = getattr(client, e)
            total, mean = endpoint(qty)
            results.append({FRAMEWORK: framework.__name__, ENDPOINT: e, TOTAL: total, MEAN: mean})

    results.sort(key=lambda e: e[MEAN])

    for e in endpoints:
        for result in results:
            if result[ENDPOINT] != e:
                continue
            table.add_row([result[c] for c in COLUMNS])
        table.add_row(["* * *" for c in COLUMNS])
    print(table)



def meow(qty: int, http: bool = False, grpc: bool = False, orm: bool = False) -> None:
    if orm:
        ask_orm(qty)
    if http:
        ask_http_requests(qty)
    if grpc:
        ask_grpc(qty)


if __name__ == '__main__':

    def extract(value: str):
        match value:
            case 'true':
                return True
            case 'false':
                return False
            case value if value.isdigit():
                return int(value)
        return False


    params = sys.argv[1:]
    print('sys params =', params)
    data = {
        "qty": 1,
        "http": False,
        "grpc": False,
        "orm": False,
    }

    for p in params:
        key, value = p.split('=')
        data[key] = extract(value)
    print('meow data =', data)
    meow(**data)

example_dict = {1: "one", 2: "two"}
inverted_dict = dict(map(reversed, example_dict.items()))
inverted_dict = {v: k for k, v in example_dict.items()}

inverted_dict = dict(map(lambda line: (line[1], line[0]), example_dict.items()))

sss = frozenset('abc')
sss |= set('efg')


