import functools
import inspect
import time
from contextlib import contextmanager
from typing import Type
from constants import SECONDS_ROUND



def duration_total_deco(seconds: float = None, exception_to_raise: Type[Exception] = TimeoutError):
    def decorator(func):
        @contextmanager
        def count_duration(func, sync):
            print(f'> {func.__name__} ({sync})')
            start_ts = time.monotonic()
            yield
            dur = time.monotonic() - start_ts
            # print(f'{sync} {func.__name__} took {str(dur)[:10]} seconds')
            print(f'  total {str(dur)[:SECONDS_ROUND]} seconds')

        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            start_ts = time.monotonic()
            result = func(*args, **kwargs)
            dur = time.monotonic() - start_ts
            return str(dur)[:SECONDS_ROUND], result

        @functools.wraps(func)
        async def async_wrapper(*args, **kwargs):
            start_ts = time.monotonic()
            result = await func(*args, **kwargs)
            dur = time.monotonic() - start_ts
            return str(dur)[:SECONDS_ROUND], result

        if inspect.iscoroutinefunction(func):
            return async_wrapper
        return wrapper

    return decorator
