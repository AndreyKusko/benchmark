import json
from copy import deepcopy

import httpx

from decorators import duration_total_deco
from utils import find_mean
from clients.base import Base, UNVICORN

import time

from data import TEST_ENGINEER


class ORMRestAPI(Base):
    """
    Выбор адреса сервера

    6 - test data
    1 - python
    1 - python interpretaor (may be pypy)
    1 - raw sql (or alchemy or django orm)
    """

    health_check = "health-check"
    engineers_path = "engineers"
    instances_ids = None

    def check_health(self):
        print('check_health')
        response = self.get(self.health_check)
        print(response.read())

    def get(self, path, instance_id=None):
        url = f"{self.host}/{path}"
        if instance_id:
            url = f"{url}/{instance_id}"
        response = httpx.get(url, headers={'Content-Type': 'application/json', "Accept": "application/json"})
        if response.status_code != 200:
            raise Exception(f'GET err response = {response} | code = {response.status_code}')
        return response

    def post(self, path, data):
        url = f"{self.host}/{path}"
        headers = {'Content-Type': 'application/json', "Accept": "application/json"}
        response = httpx.post(url, data=data, headers=headers)
        if response.status_code not in {200, 201}:
            raise Exception(f'POST err response = {response} | code = {response.status_code}')
        return response

    def put(self, path, instance_id, data):
        url = f"{self.host}/{path}/{instance_id}"
        headers = {'Content-Type': 'application/json', "Accept": "application/json"}
        response = httpx.put(url, data=data, headers=headers)
        if response.status_code != 200:
            raise Exception(f'PUT err response = {response} | code = {response.status_code}')
        return response

    def delete(self, path, instance_id):
        url = f"{self.host}/{path}/{instance_id}"
        headers = {'Content-Type': 'application/json', "Accept": "application/json"}
        response = httpx.delete(url, headers=headers)
        if response.status_code not in {204, 200}:
            raise Exception(f'PUT err response = {response} | code = {response.status_code}')
        return response

    @duration_total_deco()
    def create(self, qty):
        """
        Запрашивает словарик

        Некоторые феймворки позволяют напрямую отправлять dictionary
        """
        durations = []
        data = json.dumps(TEST_ENGINEER)
        for i in range(qty):
            start_ts = time.monotonic()
            _response = self.post(self.engineers_path, data=data)
            _response_data = _response.json()
            self.instances_ids.append(_response_data['id'])
            # print('create =', _response.json())
            durations.append(time.monotonic() - start_ts)
        return find_mean(durations)

    @duration_total_deco()
    def retrieve(self, qty):
        durations = []
        for instance_id in self.instances_ids:
            start_ts = time.monotonic()
            _response = self.get(self.engineers_path, instance_id=instance_id)
            durations.append(time.monotonic() - start_ts)
        return find_mean(durations)

    @duration_total_deco()
    def list(self, qty):
        durations = []
        for _ in range(qty):
            start_ts = time.monotonic()
            _response = self.get(self.engineers_path)
            # print('_response =', _response.json())
            durations.append(time.monotonic() - start_ts)
        return find_mean(durations)

    @duration_total_deco()
    def change(self, qty):
        durations = []
        new_data = deepcopy(TEST_ENGINEER)
        new_data['company'] = "New one"
        data = json.dumps(new_data)
        for instance_id in self.instances_ids:
            start_ts = time.monotonic()
            _response = self.put(self.engineers_path, instance_id=instance_id, data=data)
            # print('_response =', _response.json())
            durations.append(time.monotonic() - start_ts)
        return find_mean(durations)

    @duration_total_deco()
    def remove(self, qty):
        durations = []
        for instance_id in self.instances_ids:
            start_ts = time.monotonic()
            _response = self.delete(self.engineers_path, instance_id=instance_id)
            durations.append(time.monotonic() - start_ts)
        return find_mean(durations)

    def clean_db(self):
        _response = self.get('clean-db')
        return


class RawSqlPython(ORMRestAPI):
    port = 6111


class PythonSqlAlchemyAsyncSession(ORMRestAPI):
    project_layer = "python/sqlalchemy_"
    port = 6112
    server = UNVICORN
    main_file = "main_async_session"
    activate = '/opt/benchmark/orm/python/sqlalchemy_/venv/bin/activate'
    # make = "orm_python_sqlalchemy_async_session"


class PiccoloPython(ORMRestAPI):
    port = 6113


class DjangoORMPython(ORMRestAPI):
    project_layer = "python/djangoorm_"
    port = 6114
    server = UNVICORN
    activate = '/opt/benchmark/orm/python/djangoorm_/venv/bin/activate'
    # activate = '/opt/benchmark/orm/python/sqlalchemy_/venv/bin/python3'
    # make = "orm_python_djangoorm"


class PythonSqlAlchemyAsyncRepo(ORMRestAPI):
    project_layer = "python/sqlalchemy_"
    port = 6115
    server = UNVICORN
    main_file = "main_async_repo"
    activate = '/opt/benchmark/orm/python/sqlalchemy_/venv/bin/activate'
    # make = "orm_python_sqlalchemy_async_repo"


class PythonSqlAlchemySyncRepo(ORMRestAPI):
    project_layer = "python/sqlalchemy_"
    port = 6116
    server = UNVICORN
    main_file = "main_sync_repo"
    activate = '/opt/benchmark/orm/python/sqlalchemy_/venv/bin/activate'
    # make = "orm_python_sqlalchemy_sync_repo"


class PythonSqlPicciloSyncRepo(ORMRestAPI):
    '''
    https://docs.litestar.dev/2/usage/databases/piccolo.html
    '''
    project_layer = "python/sqlalchemy_"
    port = 6116
    server = UNVICORN
    main_file = "main_sync_repo"
    activate = '/opt/benchmark/orm/python/sqlalchemy_/venv/bin/activate'
    # make = "orm_python_sqlalchemy_sync_repo"
