import json
import time

import httpx

from clients.grpc_python.client import call_service, call_service_with_single_channel, \
    call_service_with_single_channel__dictionary
from data import TEST_DATA
from decorators import duration_total_deco
from utils import find_mean
import time
import grpc
import time
import inspect
import statistics

import clients.grpc_python.item_pb2 as item_pb2
import clients.grpc_python.item_pb2_grpc as item_pb2_grpc

class Base:

    def __init__(self):
        print(self.__class__.__name__, "()")

    def prepare_environment(self):
        "python3.10 -m venv /opt/benchmark/grpc_python/venv"


class RestAPI(Base):
    HOST = None

    def __init__(self):
        if not self.HOST:
            raise Exception('HOST ATTR REQUIRED!')
        super().__init__()

    def get(self, path):
        url = f"{self.HOST}/{path}"
        # response = requests.get(url, headers={'Content-Type': 'application/json', "Accept": "application/json"})
        response = httpx.get(url, headers={'Content-Type': 'application/json', "Accept": "application/json"})
        if response.status_code != 200:
            raise Exception('POST err')
        return response

    def post(self, path, data):
        url = f"{self.HOST}/{path}"
        # response = requests.post(
        response = httpx.post(
            url,
            data=data,
            headers={'Content-Type': 'application/json', "Accept": "application/json"}
        )
        if response.status_code != 200:
            raise Exception('POST err')
        return response

    @duration_total_deco()
    def dictionary(self, qty):
        """
        Запрашивает словарик

        Некоторые феймворки позволяют напрямую отправлять dictionary
        """
        durations = []
        for i in range(qty):
            start_ts = time.monotonic()
            _response = self.get("dictionary")
            durations.append(time.monotonic() - start_ts)
        return find_mean(durations)

    @duration_total_deco()
    def response__dict(self, qty):
        """
        На той стороне в  вьюшке сразу вернется Response with dict

        Пример:
            Response(data={"my_key": "my_value"})
        """
        durations = []
        for _ in range(qty):
            start_ts = time.monotonic()
            _response = self.get('response-dictionary')
            durations.append(time.monotonic() - start_ts)
        return find_mean(durations)

    @duration_total_deco()
    def serialization(self, qty):
        """
        На той стороне в данные сериализуются с помощью популярной технологии для этого фреймворка

        Пример:
            Response(data={"my_key": "my_value"})
        """
        durations = []
        for _ in range(qty):
            start_ts = time.monotonic()
            _response = self.get('serialize')
            durations.append(time.monotonic() - start_ts)
        return find_mean(durations)

    @duration_total_deco()
    def deserialize_n_serialize(self, qty):
        durations = []
        for _ in range(qty):
            start_ts = time.monotonic()
            _response = self.post('deserialize-n-serialize', data=json.dumps(TEST_DATA))
            durations.append(time.monotonic() - start_ts)
        return find_mean(durations)


class FastApiPython(RestAPI):
    HOST = "http://127.0.0.1:8101"


class FastApiPypy(RestAPI):
    HOST = 'http://127.0.0.1:8102'


class LitestarPython(RestAPI):
    HOST = 'http://127.0.0.1:8201'


class DjangoRestPython(RestAPI):
    HOST = 'http://127.0.0.1:8301'

    def dictionary(self, qty):
        return "", ""


class BlackSheepPython(RestAPI):
    source = "https://www.neoteroi.dev/blacksheep/"
    HOST = 'http://127.0.0.1:8401'


class AIOHTTPPython(RestAPI):
    HOST = 'http://127.0.0.1:8501'

    def dictionary(self, qty):
        return "", ""


class SanicPython(RestAPI):
    source = "https://sanic.dev/"
    HOST = 'http://127.0.0.1:8601'


class FalconPython(RestAPI):
    source = "https://falcon.readthedocs.io/en/stable/"
    HOST = 'http://127.0.0.1:8701'


class GRPS(Base):
    HOST = None

    @duration_total_deco()
    def with_reopen_channel__string(self, qty):
        durations = []
        for _ in range(qty):
            start_ts = time.monotonic()
            _response = call_service(self.HOST)
            durations.append(time.monotonic() - start_ts)
        return find_mean(durations)

    @duration_total_deco()
    def with_single_channel__string(self, qty):
        durations = []

        with grpc.insecure_channel(self.HOST) as channel:
            for _ in range(qty):
                start_ts = time.monotonic()
                _response = call_service_with_single_channel(channel)
                durations.append(time.monotonic() - start_ts)
        return find_mean(durations)

    @duration_total_deco()
    def with_single_channel__dictionary(self, qty):
        durations = []
        with grpc.insecure_channel(self.HOST) as channel:
            for _ in range(qty):
                start_ts = time.monotonic()
                _response = call_service_with_single_channel__dictionary(channel)
                durations.append(time.monotonic() - start_ts)
        return find_mean(durations)

    # @duration_total_deco()
    # def with_single_channel__orjson(self, qty):
    #     durations = []
    #     with grpc.insecure_channel(self.HOST) as channel:
    #         for _ in range(qty):
    #             start_ts = time.monotonic()
    #             response = call_service_with_single_channel__orjson(channel)
    #             durations.append(time.monotonic() - start_ts)
    #     return find_mean(durations)


class GRPC_Python(GRPS):
    HOST = 'localhost:7101'


class GRPC_Pypy(GRPS):
    HOST = 'localhost:7102'
