from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import grpc

import clients.grpc_python.item_pb2 as item_pb2
import clients.grpc_python.item_pb2_grpc as item_pb2_grpc


def call_service(host):
    """Main entrance."""

    # NOTE(gRPC Python Team): .close() is possible on a channel and should be
    # used in circumstances in which the with statement does not fit the needs
    # of the code.
    with grpc.insecure_channel(host) as channel:
        stub = item_pb2_grpc.ItemServiceStub(channel)
        return stub.string(item_pb2.ItemRequest(title='you'))


def call_service_pypy(host):
    """Main entrance."""

    # NOTE(gRPC Python Team): .close() is possible on a channel and should be
    # used in circumstances in which the with statement does not fit the needs
    # of the code.
    with grpc.insecure_channel(host) as channel:
        stub = item_pb2_grpc.ItemServiceStub(channel)
        return stub.string(item_pb2.ItemRequest(title='you'))


def call_service_with_single_channel(channel):
    """Main entrance."""

    # NOTE(gRPC Python Team): .close() is possible on a channel and should be
    # used in circumstances in which the with statement does not fit the needs
    # of the code.
    stub = item_pb2_grpc.ItemServiceStub(channel)
    return stub.string(item_pb2.ItemRequest(title='you'))


def call_service_with_single_channel__dictionary(channel):
    """Main entrance."""

    # NOTE(gRPC Python Team): .close() is possible on a channel and should be
    # used in circumstances in which the with statement does not fit the needs
    # of the code.
    stub = item_pb2_grpc.ItemServiceStub(channel)
    return stub.dictionary(item_pb2.ItemRequest(title='you'))


def call_service_with_single_channel__orjson(channel):
    """Main entrance."""

    # NOTE(gRPC Python Team): .close() is possible on a channel and should be
    # used in circumstances in which the with statement does not fit the needs
    # of the code.
    stub = item_pb2_grpc.ItemServiceStub(channel)
    return stub.orjson(item_pb2.ItemRequest(title='you'))

