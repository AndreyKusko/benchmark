import os
import subprocess
import time


class FolderRequired(Exception):
    ...


UNVICORN = "uvicorn"


class Base:
    port = 8000
    host = ""

    # env_base = "/opt/benchmark/"
    project_env_layer = None  # frameworks/python/
    project_env = None  # venv, pyvenv
    project_name = None  # blacksheep_

    env_path = None  # env_base+env_layer+project_name+venv -> /opt/benchmark/frameworks/python/blacksheep_/venv

    name = None  # blacksheep
    project_path = None

    projects_layer = 'orm'  # clients path
    project_layer = None  # python/sqlachemy # without forward slash
    projects_root_path = None

    server = UNVICORN

    main_file = "main"
    activate = '/opt/benchmark/orm/python/sqlalchemy_/venv/bin/python3'

    start_server_command = ""
    stop_server_command = ""

    # def __init__(self):
    #     print(self.__class__.__name__, "()")
    #     self.prepare_evironment()
    #     # if not self.project_env_path:
    #     raise FolderRequired("project_env_path required")
    # if not self.porject_path:
    #     raise FolderRequired("porject_path required")
    #
    # def prepare_evironment(self):
    #     self.name = "blacksheep"
    #     self.project_env_path = "/opt/benchmark/blacksheep"
    #     """
    #     mkdir -p /opt/benchmark
    #     mkdir -p /opt/benchmark/blacksheep
    #     python3.10 -m venv /opt/benchmark/blacksheep/venv
    #     """

    def __init__(self):
        print(f'init {self.__class__.__name__}')
        self.host = f'http://127.0.0.1:{self.port}'
        if not self.host:
            raise Exception('host attr required!')
        self.instances_ids = list()
        if self.projects_root_path:
            raise Exception('this sets automatically do not touch it')

        if not self.project_layer:
            raise Exception('project_path attr required')
        self.projects_root_path = os.path.join(self.projects_layer, self.project_layer)
        self.make_start_server_command()
        self.make_stop_server_command()
        super().__init__()

    def make_start_server_command(self):
        if self.server == UNVICORN:
            # ps ax | grep main.py
            # self.start_server_command = f"nohup . {self.activate} uvicorn --app-dir {self.projects_root_path} {self.main_file}:app --port {self.port}"
            # self.start_server_command ="nohup . /opt/benchmark/orm/python/djangoorm_/venv/bin/activate  orm/python/djangoorm_/main.py --port 6114"
            self.start_server_command ="nohup /opt/benchmark/orm/python/djangoorm_/venv/bin/python  orm/python/djangoorm_/main.py --port 6114"
            # self.start_server_command = f"nohup {self.activate} uvicorn --app-dir {self.projects_root_path} {self.main_file}:app --port {self.port}"
            # self.start_server_command = f"{self.activate} uvicorn --app-dir {self.projects_root_path} {self.main_file}:app --port {self.port}"
            # self.start_server_command = f"nohup make --file=../Makefile {self.make}"
            # " make --file=../Makefile orm_python_djangoorm"

    def make_stop_server_command(self):
        self.stop_server_command = f"kill -9 $(lsof -t -i tcp:{self.port})"

    def start_server(self):
        raise Exception('investiogation required stgrt with makefile commands for a while, watch readme')
        print('start_server |', self.start_server_command)
        # r = subprocess.getstatusoutput(self.start_server_command)
        # r = subprocess.call(self.start_server_command, shell=True)
        r = subprocess.Popen(self.start_server_command, shell=True)
        print('r =', r)
        time.sleep(3)

    def stop_server(self):
        print('stop_server |', self.stop_server_command)
        # r =subprocess.getstatusoutput(self.stop_server_command)
        # r =subprocess.call(self.stop_server_command, shell=True)
        r =subprocess.Popen(self.stop_server_command, shell=True)
        print('r =', r)

