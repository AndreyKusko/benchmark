import statistics

from constants import SECONDS_ROUND


def find_mean(durations):
    # print(f'  avg {str(statistics.mean(durations))[:SECONDS_ROUND]} seconds')
    # return statistics.mean(durations)
    return str(statistics.mean(durations))[:SECONDS_ROUND]
