# проект для замеров всякой хурмы. 


каждая папка в корневой папке это отдельное приложение. 


# для замера в caller запустить python3 main.py 

    make caller 

or
    
    cd caller
    source /opt/benchmark/caller/venv/bin/activate
    pip install -r requirements.txt
    python3 main.py 



# Создать окружения и зависимости
на каждое приложение нужно создавать свои собственные виртуальные окружения и устанавливать зависимсоти.

services

    make prepare_caller
    make prepare_fastapi_python
    make prepare_fastapi_pypy
    make prepare_fastapi_grpc_python
    make prepare_fastapi_grpc_pypy
    make prepare_litestar_python
    make prepare_blacksheep_python
    make prepare_aiohttp_python


ORM

    make prepare_aiohttp_python
    make prepare_orm_python_sqlalchemy


### Django REST
нужно еще создать бд

    make prepare_django_rest_python
    psql
    create database benchmark_django_rest;

## Запустить приложения

Все приложения запускаются в отдельных терминалах

http запросы запускаются на порте, начинающегося с 8, 8101, 8102, 8201
grpc запросы запускаются на порте, начинающегося с 18, пример 7101, 7102

далее номер сервиса 8102 - здесь - 1
в конце вариант запущенного сервиса 8101 - здесь 2, 1 - запущенный на ptyhon3.10, 2 - запущенный на pypy

### приложение на fastapi

    make fastapi_python
or 

    cd fastapi_
    source /opt/benchmark/fastapi/venv/bin/activate
    python -m pip install -r requirements.txt
    uvicorn main:app --port 8101

#### pypy

    make fastapi_pypy

or 

    cd fastapi_
    source /opt/benchmark/fastapi/venvpypy/bin/activate
    pypy3 -m pip install -r requirements_pypy.txt
    uvicorn main:app --reload --port 8102

### grpc

создать файлы item_pb2.py и item_pb2_grpc.py

    python -m grpc_tools.protoc -I./protos --python_out=. --grpc_python_out=. protos/item.proto

Здесь у меня одинаковый код для разных интерпретаторов питона

    поэтому я генерирую 1 файлы из протофайла, но обращаюсь по разным портам
см код caller clients

#### python

    make grpc_python

or

    cd grpc_python
    source /opt/benchmark/grpc_python/venv/bin/activate
    pip install -r requirements.txt
	python server.py --port 7101

#### grpc_pypy

    make grpc_pypy
or 

    cd grpc_python
    source /opt/benchmark/grpc_python/venvpypy/bin/activate
    pip install -r requirements.txt
	python server_pypy.py --port 7102
 

### litestar_

    make litestar_python

or

    cd litestar_
    source /opt/benchmark/litestar/venv/bin/activate
    pip install -r requirements.txt
    uvicorn main:app --reload --port 8201
    
### django_rest_
    
    make django_rest_python

or

    cd django_rest_
    source /opt/benchmark/django_rest/venv/bin/activate
    pip install -r requirements.txt
    uvicorn main.asgi:application --reload --port 8301

    uvicorn app:app --reload


### blacksheep_

    make blacksheep_python

or

    cd blacksheep_
    source /opt/benchmark/blacksheep/venv/bin/activate
    pip install -r requirements.txt
    uvicorn main:app --reload --port 8401


### aiohttp_

    make aiohttppython

or 

    cd aiohttp
    source /opt/benchmark/aiohtt/venv/bin/activate
    pip install -r requirements.txt
    uvicorn main:app --reload --port 8501

# Для теста ORM

### Запустить сервера
из корневой в разных терминалах запускай проекты

    make orm_python_djangoorm
    make orm_python_sqlalchemy_async_session
    make orm_python_sqlalchemy_sync_repo
    make orm_python_sqlalchemy_async_repo

### Сделать замер этой командой в новом терминале
QTY - количество

    QTY=10 ORM=true make caller
    QTY=1000 ORM=true make caller

см полную команду в Makefile make caller

вызов функций лежит в папке caller_/main.py 
внизу функция meow

# Результат на mackbook pro m1, 1_000 запросов

Все запросы синхронные, в смысле по очереди
Помни, что у каждого фреймворка свои преимущества

1000
```
+--------+-------------+-------------------------+------------------+------------+--------------+
| method | parallelism |         endpoint        |    framework     | total secs | secs/request |
+--------+-------------+-------------------------+------------------+------------+--------------+
|  GET   |     sync    |        dictionary       |  AIOHTTPPython   |            |              |
|  GET   |     sync    |        dictionary       | DjangoRestPython |            |              |
|  GET   |     sync    |        dictionary       | BlackSheepPython |  5.124291  |   0.005123   |
|  GET   |     sync    |        dictionary       |  LitestarPython  |  5.222297  |   0.005221   |
|  GET   |     sync    |        dictionary       |  FastApiPython   |  5.413169  |   0.005412   |
|  GET   |     sync    |        dictionary       |   FastApiPypy    |  7.193871  |   0.007192   |
| * * *  |    * * *    |          * * *          |      * * *       |   * * *    |    * * *     |
|  GET   |     sync    |      response__dict     |  FastApiPython   |  5.327874  |   0.005327   |
|  GET   |     sync    |      response__dict     |  LitestarPython  |  5.378607  |   0.005377   |
|  GET   |     sync    |      response__dict     |  AIOHTTPPython   |  5.403293  |   0.005402   |
|  GET   |     sync    |      response__dict     | BlackSheepPython |  5.406751  |   0.005406   |
|  GET   |     sync    |      response__dict     |   FastApiPypy    |  6.036162  |   0.006035   |
|  GET   |     sync    |      response__dict     | DjangoRestPython |  7.784525  |   0.007783   |
| * * *  |    * * *    |          * * *          |      * * *       |   * * *    |    * * *     |
|  GET   |     sync    |      serialization      | BlackSheepPython |  5.127484  |   0.005126   |
|  GET   |     sync    |      serialization      |  LitestarPython  |  5.416662  |   0.005416   |
|  GET   |     sync    |      serialization      |  FastApiPython   |  5.657120  |   0.005656   |
|  GET   |     sync    |      serialization      |  AIOHTTPPython   |  5.879636  |   0.005878   |
|  GET   |     sync    |      serialization      |   FastApiPypy    |  6.388356  |   0.006387   |
|  GET   |     sync    |      serialization      | DjangoRestPython |  8.022114  |   0.008021   |
| * * *  |    * * *    |          * * *          |      * * *       |   * * *    |    * * *     |
|  POST  |     sync    | deserialize_n_serialize | BlackSheepPython |  5.250489  |   0.005249   |
|  POST  |     sync    | deserialize_n_serialize |  LitestarPython  |  5.275993  |   0.005275   |
|  POST  |     sync    | deserialize_n_serialize |  FastApiPython   |  5.803271  |   0.005802   |
|  POST  |     sync    | deserialize_n_serialize |  AIOHTTPPython   |  5.868179  |   0.005867   |
|  POST  |     sync    | deserialize_n_serialize |   FastApiPypy    |  6.700270  |   0.006699   |
|  POST  |     sync    | deserialize_n_serialize | DjangoRestPython |  8.264262  |   0.008262   |
+--------+-------------+-------------------------+------------------+------------+--------------+
```

grpc 1000
```
+---------------------------------+-------------+------------+--------------+
|             endpoint            |  framework  | total secs | secs/request |
+---------------------------------+-------------+------------+--------------+
|   with_reopen_channel__string   |  GRPC_Pypy  |  2.801716  |   0.002800   |
|   with_reopen_channel__string   | GRPC_Python |  2.909989  |   0.002908   |
|              * * *              |    * * *    |   * * *    |    * * *     |
|   with_single_channel__string   | GRPC_Python |  0.198186  |   0.000197   |
|   with_single_channel__string   |  GRPC_Pypy  |  0.433144  |   0.000432   |
|              * * *              |    * * *    |   * * *    |    * * *     |
| with_single_channel__dictionary | GRPC_Python |  0.303845  |   0.000302   |
| with_single_channel__dictionary |  GRPC_Pypy  |  0.349818  |   0.000348   |
+---------------------------------+-------------+------------+--------------+
```

orm testing 1000

```
+--------+-------------+----------+------------------------------+------------+--------------+
| method | parallelism | endpoint |          framework           | total secs | secs/request |
+--------+-------------+----------+------------------------------+------------+--------------+
|  POST  |     sync    |  create  |       DjangoORMPython        |  6.281473  |   0.006280   |
|  POST  |     sync    |  create  |  PythonSqlAlchemyAsyncRepo   |  7.375257  |   0.007374   |
|  POST  |     sync    |  create  | PythonSqlAlchemyAsyncSession |  8.114800  |   0.008114   |
|  POST  |     sync    |  create  |   PythonSqlAlchemySyncRepo   |  8.460742  |   0.008460   |
| * * *  |    * * *    |  * * *   |            * * *             |   * * *    |    * * *     |
|  GET   |     sync    | retrieve |       DjangoORMPython        |  5.824013  |   0.005823   |
|  GET   |     sync    | retrieve |   PythonSqlAlchemySyncRepo   |  6.551469  |   0.006550   |
|  GET   |     sync    | retrieve |  PythonSqlAlchemyAsyncRepo   |  6.554344  |   0.006553   |
|  GET   |     sync    | retrieve | PythonSqlAlchemyAsyncSession |  7.110087  |   0.007109   |
| * * *  |    * * *    |  * * *   |            * * *             |   * * *    |    * * *     |
|  PUT   |     sync    |  change  |       DjangoORMPython        |  6.862515  |   0.006861   |
|  PUT   |     sync    |  change  | PythonSqlAlchemyAsyncSession |  7.946368  |   0.007945   |
|  PUT   |     sync    |  change  |  PythonSqlAlchemyAsyncRepo   |  8.424764  |   0.008424   |
|  PUT   |     sync    |  change  |   PythonSqlAlchemySyncRepo   |  8.863784  |   0.008863   |
| * * *  |    * * *    |  * * *   |            * * *             |   * * *    |    * * *     |
|  GET   |     sync    |   list   |       DjangoORMPython        |  74.21153  |   0.074210   |
|  GET   |     sync    |   list   |  PythonSqlAlchemyAsyncRepo   |  79.48255  |   0.079481   |
|  GET   |     sync    |   list   | PythonSqlAlchemyAsyncSession |  81.86374  |   0.081862   |
|  GET   |     sync    |   list   |   PythonSqlAlchemySyncRepo   |  82.00103  |   0.082000   |
| * * *  |    * * *    |  * * *   |            * * *             |   * * *    |    * * *     |
| DELETE |     sync    |  remove  |       DjangoORMPython        |  6.347849  |   0.006347   |
| DELETE |     sync    |  remove  |  PythonSqlAlchemyAsyncRepo   |  6.609676  |   0.006609   |
| DELETE |     sync    |  remove  |   PythonSqlAlchemySyncRepo   |  7.010236  |   0.007009   |
| DELETE |     sync    |  remove  | PythonSqlAlchemyAsyncSession |  7.480939  |   0.007480   |
+--------+-------------+----------+------------------------------+------------+--------------+
```

