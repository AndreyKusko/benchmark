
from litestar import post, Controller, put, delete

from litestar.plugins.sqlalchemy import SQLAlchemyInitPlugin, repository

from litestar.params import Parameter
from litestar.repository.filters import LimitOffset
from litestar.di import Provide
import typing as tp

from litestar import Litestar, get
from litestar.contrib.sqlalchemy.plugins import SQLAlchemyPlugin, \
    SQLAlchemySyncConfig

from sqlalchemy import select, delete as sqlalchemy_delete
from humans.models.engineers import EngineerModel
from humans.schemas import EngineerSchema
from pydantic import BaseModel as _BaseModel

from sqlalchemy.orm import Session


class BaseModel(_BaseModel):
    """Extend Pydantic's BaseModel to enable ORM mode"""

    model_config = {"from_attributes": True}


sqlalchemy_config = SQLAlchemySyncConfig(
    connection_string="postgresql://postgres:@localhost:5432/benchmark_python_sqlalchemy")  # Create 'db_session' dependency.
sqlalchemy_plugin = SQLAlchemyInitPlugin(config=sqlalchemy_config)


class EngineerRepository(repository.SQLAlchemySyncRepository[EngineerModel]):
    """Author repository."""

    model_type = EngineerModel


class Engineer(BaseModel):
    about: str
    address: str
    age: int
    balance: float
    company: str
    id: tp.Optional[int]
    email: str
    eyeColor: str
    favorite: str
    friends: list
    gender: str
    greeting: str
    guid: str
    index: int
    isActive: bool
    latitude: float
    longitude: float
    name: str
    phone: str
    picture: str
    tags: list



class EngineerCreateUpdate(BaseModel):
    about: str
    address: str
    age: int
    balance: float
    company: str
    email: str
    eyeColor: str
    favorite: str
    friends: list
    gender: str
    greeting: str
    guid: str
    index: int
    isActive: bool
    latitude: float
    longitude: float
    name: str
    phone: str
    picture: str
    tags: list


def provide_engineers_repo(db_session: Session) -> EngineerRepository:
    """This provides the default Authors repository."""
    return EngineerRepository(session=db_session)


# we can optionally override the default `select` used for the repository to pass in
# specific SQL options such as join details
def provide_engineer_details_repo(db_session: Session) -> EngineerRepository:
    """This provides a simple example demonstrating how to override the join options for the repository."""
    return EngineerRepository(
        statement=select(EngineerModel)
        # .options(selectinload(EngineerModel.books))
        ,
        session=db_session,
    )


@get("/health-check", status_code=200, sync_to_thread=True)
def health_check() -> str:
    return "I'm alive"


@get("/favicon.ico", status_code=200, sync_to_thread=True)
def favicon() -> str:
    return ""


@get("/clean-db", status_code=200)
def clean_db( db_session: Session)-> None:
    query = sqlalchemy_delete(EngineerModel)
    db_session.execute(query)
    db_session.commit()
    return


def provide_limit_offset_pagination(
        current_page: int = Parameter(ge=1, query="currentPage", default=1, required=False),
        page_size: int = Parameter(
            query="pageSize",
            ge=1,
            default=10,
            required=False,
        ),
) -> LimitOffset:
    """Add offset/limit pagination.

    Return type consumed by `Repository.apply_limit_offset_pagination()`.

    Parameters
    ----------
    current_page : int
        LIMIT to apply to select.
    page_size : int
        OFFSET to apply to select.
    """
    return LimitOffset(page_size, page_size * (current_page - 1))


class EngineerController(Controller):
    path = "/engineers"
    dependencies = {"engineers_repo": Provide(provide_engineers_repo, sync_to_thread=True)}

    @get(sync_to_thread=True)
    def list(self, engineers_repo: EngineerRepository) -> list[EngineerSchema]:
        """
        curl --location --request GET 'http://127.0.0.1:6114/items/' --header 'Accept: application/json' --header 'Content-Type: application/json' \
        """
        results = engineers_repo.list()
        result = [EngineerSchema.from_orm(r) for r in results]
        return result

    @post(sync_to_thread=True)
    def create(self, engineers_repo: EngineerRepository, data: EngineerCreateUpdate) -> Engineer:
        """Create."""
        obj = engineers_repo.add(EngineerModel(**data.model_dump(exclude_unset=True, exclude_none=True)), )
        engineers_repo.session.commit()
        return Engineer.model_validate(obj)

    @put(
        path="/{instance_id:int}",
        dependencies={"engineers_repo": Provide(provide_engineer_details_repo, sync_to_thread=True)},
        sync_to_thread=True
    )
    def change(
            self,
            engineers_repo: EngineerRepository,
            data: EngineerCreateUpdate,
            instance_id: int = Parameter(
                title="ID",
                description="The instance to delete.",
            )
    ) -> Engineer:
        raw_obj = data.model_dump(exclude_unset=True, exclude_none=True)
        raw_obj.update({"id": instance_id})
        obj = engineers_repo.update(EngineerModel(**raw_obj))
        engineers_repo.session.commit()
        return Engineer.model_validate(obj)

    @get(
        path="/{instance_id:int}",
        dependencies={"engineers_repo": Provide(provide_engineer_details_repo, sync_to_thread=True)},
        sync_to_thread=True
    )
    def extract(self, engineers_repo: EngineerRepository, instance_id: int) -> Engineer:
        obj = engineers_repo.get(instance_id)
        return Engineer.model_validate(obj)

    @delete(path="/{instance_id:int}", code=204,
            sync_to_thread=True
            )
    def remove(
            self, engineers_repo: EngineerRepository,
            instance_id: int = Parameter(
                title="ID",
                description="The instance to delete.",
            ),
    ) -> None:
        """Delete an instance from the system."""
        _ = engineers_repo.delete(instance_id)
        engineers_repo.session.commit()


"""
source /opt/benchmark/orm/python/sqlalchemy_/venv/bin/activate



https://docs.litestar.dev/2/usage/databases/sqlalchemy/models_and_repository.html#sqlalchemy-models-repository
# референс
def create_author(  

python3 main.py --port 6115
uvicorn main:app --reload --port 6115

uvicorn --app-dir orm/python/sqlalchemy_ main_async_repo:app --reload --port 6116;;
uvicorn main_sync_repo:app --reload --port 6116;
"""

app = Litestar(
    route_handlers=[EngineerController, health_check, favicon, clean_db],
    debug=True,
    plugins=[SQLAlchemyPlugin(config=sqlalchemy_config)],
)
