
from litestar import post, Controller, put, delete


from sqlalchemy import select, delete as sqlalchemy_delete
from sqlalchemy.ext.asyncio import AsyncEngine, AsyncSession

from litestar import Litestar, get
from litestar.contrib.sqlalchemy.plugins import AsyncSessionConfig, SQLAlchemyAsyncConfig, SQLAlchemyPlugin

from humans.models.engineers import EngineerModel
from humans.schemas import EngineerSchema


session_config = AsyncSessionConfig(expire_on_commit=False)
sqlalchemy_config = SQLAlchemyAsyncConfig(
    connection_string="postgresql+asyncpg://postgres:@localhost:5432/benchmark_python_sqlalchemy",
    session_config=session_config, create_all=True
)  # Create 'async_session' dependency.


@get("/health-check", status_code=200)
async def health_check() -> str:
    return "I'm alive"

@get("/favicon.ico", status_code=200)
async def favicon() -> str:
    return ""


@get("/clean-db", status_code=200)
async def clean_db( db_session: AsyncSession)-> None:
    query = sqlalchemy_delete(EngineerModel)
    await db_session.execute(query)
    await db_session.commit()
    return


class EngineerController(Controller):
    path = "/engineers"

    @post()
    async def create(
            self, data: EngineerSchema, db_session: AsyncSession, db_engine: AsyncEngine
    ) -> EngineerSchema:
        """
        curl --location 'http://127.0.0.1:6114/engineers/11' --header 'Accept: application/json' --header 'Content-Type: application/json' --header 'Cookie: csrftoken=JoI6q79eJ2upJhGkOEpMH9hmGoWZ3QX4' --data-raw '{ "about": "I’m currently a Senior Data Science Manager at Indeed.com, where I help our Job Search Front End, Search Matching/Ranking, and Taxonomy teams.", "address": "087 Simmons Greens Apt. 948\nGinastad, LA 61807", "age": 32, "balance": 5583.41, "birthday": "1993-11-16 19:18:24.000256", "company": "Indeed.com", "email": "lisahenson@indeed.com", "eyeColor": "#e2b21f", "favorite": "whole", "friends": [{"id": 63, "name": "Derek Greene"}, {"id": 68, "name": "Renee Doyle"}, {"id": 36, "name": "James Livingston"}, {"id": 43, "name": "Tyler Murphy"}], "gender": "female", "greeting": "Howdy!", "guid": "749a6412-63a0-47f8-bdd8-268bd5c2162b", "index": 2566, "isActive": 0, "latitude": 36.215487, "longitude": 71.038439, "name": "Lisa Henson", "phone": "791.126.7836x392", "picture": "https://somehost.io/37x29", "registered": "2019-11-16 19:18:24.000256", "tags": ["data science", "internet", "california", "bike", "lakers", "food", "travelling", "hiking"] }'
        """
        instance = EngineerModel(**data.dict())
        db_session.add(instance)
        await db_session.commit()
        # statement = select(func.count()).select_from(Author)
        # count = await session.execute(statement)
        return EngineerSchema.from_orm(instance)

    @get()
    async def list(self, db_session: AsyncSession, db_engine: AsyncEngine) -> EngineerSchema:
        """
        curl --location --request GET 'http://127.0.0.1:6114/items/' --header 'Accept: application/json' --header 'Content-Type: application/json' \
        """
        query = select(EngineerModel)
        query = query.fetch(1000)

        instances = await db_session.execute(query)
        instances = instances.scalars().all()
        result = [EngineerSchema.from_orm(item) for item in instances]
        return result

    @put(path="/{instance_id:int}")
    async def change(
            self, instance_id: int, data: EngineerSchema, db_session: AsyncSession, db_engine: AsyncEngine
    ) -> EngineerSchema:
        data = data.dict()
        data.pop('id')

        query = select(EngineerModel)
        query = query.filter(EngineerModel.id == instance_id)
        instance = await db_session.execute(query)
        instance = instance.scalar_one()
        for key, value in data.items():
            setattr(instance, key, value)
        await db_session.commit()

        return EngineerSchema.from_orm(instance)

    @get(path="/{instance_id:int}")
    async def extract(self, instance_id: int, db_session: AsyncSession, db_engine: AsyncEngine) -> EngineerSchema:
        query = select(EngineerModel)
        query = query.where(EngineerModel.id == instance_id)
        instance = await db_session.execute(query)
        instance = instance.scalar_one()
        return EngineerSchema.from_orm(instance)

    @delete(path="/{instance_id:int}", code=204)
    async def remove(self, instance_id: int, db_session: AsyncSession, db_engine: AsyncEngine) -> None:
        query = select(EngineerModel)
        query = query.where(EngineerModel.id == instance_id)
        row = await db_session.execute(query)
        row = row.scalar_one()
        await db_session.delete(row)
        await db_session.commit()
        return

"""
source /opt/benchmark/orm/python/sqlalchemy_/venv/bin/activate

session
https://docs.litestar.dev/2/usage/databases/sqlalchemy/models_and_repository.html#sqlalchemy-models-repository

python3 main.py --port 6112
uvicorn main:app --reload --port 6112

uvicorn --app-dir orm/python/sqlalchemy_ main_async_session:app --reload --port 6112;
uvicorn main_async_session:app --reload --port 6112;
"""
app = Litestar(
    route_handlers=[EngineerController, health_check, favicon, clean_db],
    debug=True,
    plugins=[SQLAlchemyPlugin(config=sqlalchemy_config)],
)
