
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Integer, Column, String, Boolean, Float
from sqlalchemy.dialects.postgresql import JSONB

from sqlalchemy.orm import Mapped

Base = declarative_base()

class EngineerModel(Base):
    __tablename__ = "python_sqlalchemy_async_session_engineer"

    id = Column(Integer, primary_key=True)
    about = Column(String(255), )
    address = Column(String(255), )
    # DateTime(timezone=True)
    age =Column(Integer())
    balance = Column(Float(asdecimal=True))
    company = Column(String(255), )
    email = Column(String(255), )
    eyeColor = Column(String(255), )
    favorite = Column(String(255), )
    friends = Column(JSONB)
    gender =  Column(String(255), )
    greeting =  Column(String(255), )
    guid =  Column(String(255),)
    index = Column(Integer, )
    isActive =  Column(Boolean)
    latitude = Column(Float, )
    longitude = Column(Float, )
    name =  Column(String(255), )
    phone =  Column(String(255), )
    picture =  Column(String(255), )
    tags = Column(JSONB)
