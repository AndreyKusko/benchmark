
from litestar import post,  Controller, put, delete

from litestar.plugins.sqlalchemy import (
    repository,
)
from typing import TYPE_CHECKING

from litestar.params import Parameter
from litestar.repository.filters import LimitOffset
from litestar.di import Provide

from litestar import Litestar, get
from litestar.contrib.sqlalchemy.plugins import AsyncSessionConfig, SQLAlchemyAsyncConfig, SQLAlchemyPlugin

from sqlalchemy import select, delete as sqlalchemy_delete

from sqlalchemy.ext.asyncio import AsyncSession
import typing as tp


from humans.models.engineers import EngineerModel
from humans.schemas import EngineerSchema
from pydantic import BaseModel as _BaseModel

if TYPE_CHECKING:
    from sqlalchemy.orm import Session


class BaseModel(_BaseModel):
    """Extend Pydantic's BaseModel to enable ORM mode"""

    model_config = {"from_attributes": True}


session_config = AsyncSessionConfig(expire_on_commit=False)
sqlalchemy_config = SQLAlchemyAsyncConfig(
    connection_string="postgresql+asyncpg://postgres:@localhost:5432/benchmark_python_sqlalchemy",
    session_config=session_config, create_all=True
)  # Create 'async_session' dependency.


class EngineerRepository(repository.SQLAlchemyAsyncRepository[EngineerModel]):
    """Author repository."""

    model_type = EngineerModel


class Engineer(BaseModel):
    about: str
    address: str
    age: int
    balance: float
    company: str
    id: tp.Optional[int]
    email: str
    eyeColor: str
    favorite: str
    friends: list
    gender: str
    greeting: str
    guid: str
    index: int
    isActive: bool
    latitude: float
    longitude: float
    name: str
    phone: str
    picture: str
    tags: list


class EngineerCreateUpdate(BaseModel):
    about: str
    address: str
    age: int
    balance: float
    company: str
    email: str
    eyeColor: str
    favorite: str
    friends: list
    gender: str
    greeting: str
    guid: str
    index: int
    isActive: bool
    latitude: float
    longitude: float
    name: str
    phone: str
    picture: str
    tags: list


async def provide_engineers_repo(db_session: AsyncSession) -> EngineerRepository:
    """This provides the default Authors repository."""
    return EngineerRepository(session=db_session)


# we can optionally override the default `select` used for the repository to pass in
# specific SQL options such as join details
async def provide_engineer_details_repo(db_session: AsyncSession) -> EngineerRepository:
    """This provides a simple example demonstrating how to override the join options for the repository."""
    return EngineerRepository(
        statement=select(EngineerModel)
        # .options(selectinload(EngineerModel.books))
        ,
        session=db_session,
    )


@get("/health-check", status_code=200)
async def health_check() -> str:
    return "I'm alive"


@get("/favicon.ico", status_code=200)
async def favicon() -> str:
    return ""


@get("/clean-db", status_code=200)
async def clean_db(db_session: AsyncSession) -> None:
    query = sqlalchemy_delete(EngineerModel)
    row = await db_session.execute(query)
    await db_session.commit()
    return


async def provide_limit_offset_pagination(
        current_page: int = Parameter(ge=1, query="currentPage", default=1, required=False),
        page_size: int = Parameter(
            query="pageSize",
            ge=1,
            default=10,
            required=False,
        ),
) -> LimitOffset:
    """Add offset/limit pagination.

    Return type consumed by `Repository.apply_limit_offset_pagination()`.

    Parameters
    ----------
    current_page : int
        LIMIT to apply to select.
    page_size : int
        OFFSET to apply to select.
    """
    return LimitOffset(page_size, page_size * (current_page - 1))


class EngineerController(Controller):
    path = "/engineers"
    dependencies = {"engineers_repo": Provide(provide_engineers_repo, sync_to_thread=False)}

    @get(sync_to_thread=False)
    # async def list(self, engineers_repo: EngineerRepository, limit_offset: LimitOffset) -> Engineer:
    async def list(self, engineers_repo: EngineerRepository) -> list[EngineerSchema]:
        """
        curl --location --request GET 'http://127.0.0.1:6114/items/' --header 'Accept: application/json' --header 'Content-Type: application/json' \
        """
        results = await engineers_repo.list()
        result = [EngineerSchema.from_orm(r) for r in results]
        return result

    @post(sync_to_thread=False)
    async def create(self, engineers_repo: EngineerRepository, data: EngineerCreateUpdate) -> Engineer:
        obj = await engineers_repo.add(EngineerModel(**data.model_dump(exclude_unset=True, exclude_none=True)), )
        await engineers_repo.session.commit()
        return Engineer.model_validate(obj)

    @put(
        path="/{instance_id:int}",
        dependencies={"engineers_repo": Provide(provide_engineer_details_repo, sync_to_thread=False)},
        sync_to_thread=False
    )
    async def change(
            self,
            engineers_repo: EngineerRepository,
            data: EngineerCreateUpdate,
            instance_id: int = Parameter(
                title="ID",
                description="The instance to delete.",
            )
    ) -> Engineer:
        raw_obj = data.model_dump(exclude_unset=True, exclude_none=True)
        raw_obj.update({"id": instance_id})
        obj = await engineers_repo.update(EngineerModel(**raw_obj))
        await engineers_repo.session.commit()
        return Engineer.model_validate(obj)

    @get(
        path="/{instance_id:int}",
        dependencies={"engineers_repo": Provide(provide_engineer_details_repo, sync_to_thread=False)},
        sync_to_thread=False
    )
    async def extract(self, engineers_repo: EngineerRepository, instance_id: int) -> Engineer:
        obj = await engineers_repo.get(instance_id)
        return Engineer.model_validate(obj)

    @get(path="/{instance_id:int}", sync_to_thread=False)
    async def extract(self, instance_id: int, engineers_repo: EngineerRepository) -> Engineer:
        obj = await engineers_repo.get(instance_id)
        return Engineer.model_validate(obj)

    @delete(path="/{instance_id:int}", code=204)
    async def remove(
            self, engineers_repo: EngineerRepository,
            instance_id: int = Parameter(
                title="ID",
                description="The instance to delete.",
            ),
    ) -> None:
        """Delete an instance from the system."""
        _ = await engineers_repo.delete(instance_id)
        await engineers_repo.session.commit()


"""
source /opt/benchmark/orm/python/sqlalchemy_/venv/bin/activate

https://docs.litestar.dev/2/usage/databases/sqlalchemy/models_and_repository.html#sqlalchemy-models-repository
# референс
async def create_author(  

python3 main.py --port 6115
uvicorn main:app --reload --port 6115

uvicorn --app-dir orm/python/sqlalchemy_ main_async_repo:app --reload --port 6115;
uvicorn main_async_repo:app --reload --port 6115;
"""

app = Litestar(
    route_handlers=[EngineerController, health_check, favicon, clean_db],
    debug=True,
    plugins=[SQLAlchemyPlugin(config=sqlalchemy_config)],
)
