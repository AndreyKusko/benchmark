import os

from asgiref.sync import sync_to_async
from litestar import Litestar, get, post, Response, Controller, patch, put, delete

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "main_files.settings")  # isort:skip
from django.conf import settings
from django.apps import apps
import uvicorn
import typing as tp

apps.populate(settings.INSTALLED_APPS)  # isort:skip

from humans.models.engineers import Engineer
from humans.schemas import EngineerSchema, EngineerCreateUpdate

"""
python3 main.py --port 6114
uvicorn main:app --reload --port 6114
"""


@get("/health-check", status_code=200)
async def health_check() -> str:
    return "I'm alive"


@get("/favicon.ico", status_code=200)
async def favicon() -> str:
    return ""


@get("/clean-db", status_code=200)
async def clean_db(*_args: tp.Optional[tp.Any], **_kwargs:tp.Optional[tp.Any]) -> None:
    Engineer.objects.all().delete()
    return


class ItemController(Controller):
    path = "/engineers"

    @post()
    async def create(self, data: EngineerCreateUpdate) -> EngineerSchema:
        """
        curl --location 'http://127.0.0.1:6114/engineers/11' --header 'Accept: application/json' --header 'Content-Type: application/json' --header 'Cookie: csrftoken=JoI6q79eJ2upJhGkOEpMH9hmGoWZ3QX4' --data-raw '{ "about": "I’m currently a Senior Data Science Manager at Indeed.com, where I help our Job Search Front End, Search Matching/Ranking, and Taxonomy teams.", "address": "087 Simmons Greens Apt. 948\nGinastad, LA 61807", "age": 32, "balance": 5583.41, "birthday": "1993-11-16 19:18:24.000256", "company": "Indeed.com", "email": "lisahenson@indeed.com", "eyeColor": "#e2b21f", "favorite": "whole", "friends": [{"id": 63, "name": "Derek Greene"}, {"id": 68, "name": "Renee Doyle"}, {"id": 36, "name": "James Livingston"}, {"id": 43, "name": "Tyler Murphy"}], "gender": "female", "greeting": "Howdy!", "guid": "749a6412-63a0-47f8-bdd8-268bd5c2162b", "index": 2566, "isActive": 0, "latitude": 36.215487, "longitude": 71.038439, "name": "Lisa Henson", "phone": "791.126.7836x392", "picture": "https://somehost.io/37x29", "registered": "2019-11-16 19:18:24.000256", "tags": ["data science", "internet", "california", "bike", "lakers", "food", "travelling", "hiking"] }'
        """
        instance = Engineer.objects.create(**data.dict())
        return EngineerSchema.from_orm(instance)

    @get()
    async def list(self) -> list[EngineerSchema]:
        """
            curl --location --request GET 'http://127.0.0.1:6114/items/' --header 'Accept: application/json' --header 'Content-Type: application/json' \
        """
        instances = Engineer.objects.all()
        result = [EngineerSchema.from_orm(item) for item in instances[0:1_000]]
        return result

    @put(path="/{instance_id:int}")
    async def change(self, instance_id: int, data: EngineerCreateUpdate) -> EngineerSchema:
        data = data.dict()
        instance = Engineer.objects.get(id=instance_id)
        for key, value in data.items():
            setattr(instance, key, value)
        instance.save()
        return EngineerSchema.from_orm(instance)

    @get(path="/{instance_id:int}")
    async def get(self, instance_id: int) -> EngineerSchema:
        instance = Engineer.objects.get(id=instance_id)
        return EngineerSchema.from_orm(instance)

    @delete(path="/{instance_id:int}", code=204)
    async def delete(self, instance_id: int) -> None:
        instance = Engineer.objects.get(id=instance_id)
        instance.delete()
        return


app = Litestar([ItemController, health_check, favicon, clean_db], debug=True, )

if __name__ == "__main__":
    uvicorn.run("main:app", port=6114, log_level="info")
