
from django.contrib.staticfiles import views

from django.conf import settings


urlpatterns = []

if settings.DEBUG:
    from django.urls import re_path

    urlpatterns += [re_path(r"^static/(?P<path>.*)$", views.serve)]
