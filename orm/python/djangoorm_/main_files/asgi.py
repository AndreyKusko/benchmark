import os

# import sentry_sdk
# from fastapi import FastAPI
# from django.apps import apps
# from django.conf import settings
# from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "main_files.settings")  # isort:skip
apps.populate(settings.INSTALLED_APPS)  # isort:skip

# if SENTRY_URL:
#     sentry_sdk.init(
#         SENTRY_URL, traces_sample_rate=1.0, environment=SERVER_ENVIRONMENT, server_name=SERVER_NAME
#     )
#
#
# def get_application() -> FastAPI:
#     app = FastAPI(title=settings.PROJECT_NAME, debug=settings.DEBUG)
#     app.add_middleware(
#         CORSMiddleware,
#         allow_origins=settings.ALLOWED_HOSTS or ["*"],
#         allow_credentials=True,
#         allow_methods=["*"],
#         allow_headers=["*"],
#     )
#     app.include_router(psr)
#     app.include_router(router)
#     app.mount("/django", WSGIMiddleware(get_wsgi_application()))
#
#     import asyncio
#
#     asyncio.ensure_future(consume_kafka_messages())
#     return app
#
#
# app = get_application()
