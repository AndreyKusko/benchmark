from django.db import models


class Engineer(models.Model):
    about = models.CharField()
    address = models.CharField()
    age = models.PositiveIntegerField()
    balance = models.DecimalField(max_digits=6, decimal_places=2)
    company = models.CharField()
    email = models.EmailField()
    eyeColor = models.CharField()
    favorite = models.CharField()
    friends = models.JSONField()
    gender = models.CharField()
    greeting = models.CharField()
    guid = models.CharField()
    index = models.PositiveIntegerField()
    isActive = models.BooleanField()
    latitude = models.FloatField()
    longitude = models.FloatField()
    name = models.CharField()
    phone = models.CharField()
    picture = models.CharField()
    tags = models.JSONField()

    class Meta:
        ordering = ("-id",)
        # db_table = "python_djangoorm_engineer"
        db_table = "python_djangoorm_sync_engineer"
