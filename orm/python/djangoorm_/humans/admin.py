
from django.contrib import admin

from humans.models.engineers import Engineer

@admin.register(Engineer)
class EngineerAdmin(admin.ModelAdmin):
    ...
