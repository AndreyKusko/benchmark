from decimal import Decimal
from typing import List, Union, Optional, Annotated

from pydantic import BaseModel, Field, EmailStr, field_validator

from django.db import models
from pydantic.types import Any

from django.db import models


class EngineerSchema(BaseModel):
    id: Optional[int] = None
    about: str
    address: str
    age: Annotated[int, Field(gt=0)]
    balance: Decimal
    company: str
    email: EmailStr
    eyeColor: str
    favorite: str
    friends: list
    gender: str
    greeting: str
    guid: str
    index: int
    isActive: bool
    latitude: Annotated[float, Field(ge=-90, le=90)]
    longitude: Annotated[float, Field(ge=-180, le=180)]
    name: str = Field(min_length=2, )
    phone: str = Field(min_length=5, )
    picture: str
    tags: list

    @field_validator("gender")
    @classmethod
    def validate_gender(cls, value):
        if value not in ["male", "female"]:
            raise ValueError("Invalid gender format")
        return value

    class Config:
        from_attributes = True


class EngineerCreateUpdate(BaseModel):
    about: str
    address: str
    age: Annotated[int, Field(gt=0)]
    balance: Decimal
    company: str
    email: EmailStr
    eyeColor: str
    favorite: str
    friends: list
    gender: str
    greeting: str
    guid: str
    index: int
    isActive: bool
    latitude: Annotated[float, Field(ge=-90, le=90)]
    longitude: Annotated[float, Field(ge=-180, le=180)]
    name: str = Field(min_length=2, )
    phone: str = Field(min_length=5, )
    picture: str
    tags: list

    @field_validator("gender")
    @classmethod
    def validate_gender(cls, value):
        if value not in ["male", "female"]:
            raise ValueError("Invalid gender format")
        return value

    class Config:
        from_attributes = True
