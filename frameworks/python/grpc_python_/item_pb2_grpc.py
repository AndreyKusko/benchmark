# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc
import warnings

import item_pb2 as item__pb2

GRPC_GENERATED_VERSION = '1.64.1'
GRPC_VERSION = grpc.__version__
EXPECTED_ERROR_RELEASE = '1.65.0'
SCHEDULED_RELEASE_DATE = 'June 25, 2024'
_version_not_supported = False

try:
    from grpc._utilities import first_version_is_lower
    _version_not_supported = first_version_is_lower(GRPC_VERSION, GRPC_GENERATED_VERSION)
except ImportError:
    _version_not_supported = True

if _version_not_supported:
    warnings.warn(
        f'The grpc package installed is at version {GRPC_VERSION},'
        + f' but the generated code in item_pb2_grpc.py depends on'
        + f' grpcio>={GRPC_GENERATED_VERSION}.'
        + f' Please upgrade your grpc module to grpcio>={GRPC_GENERATED_VERSION}'
        + f' or downgrade your generated code using grpcio-tools<={GRPC_VERSION}.'
        + f' This warning will become an error in {EXPECTED_ERROR_RELEASE},'
        + f' scheduled for release on {SCHEDULED_RELEASE_DATE}.',
        RuntimeWarning
    )


class ItemServiceStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.string = channel.unary_unary(
                '/helloworld.ItemService/string',
                request_serializer=item__pb2.ItemRequest.SerializeToString,
                response_deserializer=item__pb2.ItemResponse.FromString,
                _registered_method=True)
        self.orjson = channel.unary_unary(
                '/helloworld.ItemService/orjson',
                request_serializer=item__pb2.ItemRequest.SerializeToString,
                response_deserializer=item__pb2.ItemResponse.FromString,
                _registered_method=True)
        self.dictionary = channel.unary_unary(
                '/helloworld.ItemService/dictionary',
                request_serializer=item__pb2.ItemRequest.SerializeToString,
                response_deserializer=item__pb2.ItemResponse.FromString,
                _registered_method=True)


class ItemServiceServicer(object):
    """Missing associated documentation comment in .proto file."""

    def string(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def orjson(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def dictionary(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_ItemServiceServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'string': grpc.unary_unary_rpc_method_handler(
                    servicer.string,
                    request_deserializer=item__pb2.ItemRequest.FromString,
                    response_serializer=item__pb2.ItemResponse.SerializeToString,
            ),
            'orjson': grpc.unary_unary_rpc_method_handler(
                    servicer.orjson,
                    request_deserializer=item__pb2.ItemRequest.FromString,
                    response_serializer=item__pb2.ItemResponse.SerializeToString,
            ),
            'dictionary': grpc.unary_unary_rpc_method_handler(
                    servicer.dictionary,
                    request_deserializer=item__pb2.ItemRequest.FromString,
                    response_serializer=item__pb2.ItemResponse.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'helloworld.ItemService', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))
    server.add_registered_method_handlers('helloworld.ItemService', rpc_method_handlers)


 # This class is part of an EXPERIMENTAL API.
class ItemService(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def string(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(
            request,
            target,
            '/helloworld.ItemService/string',
            item__pb2.ItemRequest.SerializeToString,
            item__pb2.ItemResponse.FromString,
            options,
            channel_credentials,
            insecure,
            call_credentials,
            compression,
            wait_for_ready,
            timeout,
            metadata,
            _registered_method=True)

    @staticmethod
    def orjson(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(
            request,
            target,
            '/helloworld.ItemService/orjson',
            item__pb2.ItemRequest.SerializeToString,
            item__pb2.ItemResponse.FromString,
            options,
            channel_credentials,
            insecure,
            call_credentials,
            compression,
            wait_for_ready,
            timeout,
            metadata,
            _registered_method=True)

    @staticmethod
    def dictionary(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(
            request,
            target,
            '/helloworld.ItemService/dictionary',
            item__pb2.ItemRequest.SerializeToString,
            item__pb2.ItemResponse.FromString,
            options,
            channel_credentials,
            insecure,
            call_credentials,
            compression,
            wait_for_ready,
            timeout,
            metadata,
            _registered_method=True)
