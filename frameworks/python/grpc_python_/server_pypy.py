import sys
from concurrent import futures
import time
from copy import deepcopy

import item_pb2_grpc

import item_pb2
import grpc

import json

TEST_DATA = {
    "_id": "801512f3ff9658d50e47fe90",
    "about": "I’m currently a Senior Data Science Manager at Indeed.com, where I help our Job Search Front End, Search Matching/Ranking, and Taxonomy teams.",
    "address": "087 Simmons Greens Apt. 948\nGinastad, LA 61807",
    "age": 32,
    "balance": "$5,583.41",
    # "birthday": str(datetime.datetime(1990, 5, 4, 0, 0, 0, 0)),
    "company": "Indeed.com",
    "email": "lisahenson@indeed.com",
    "eyeColor": "#e2b21f",
    "favorite": "whole",
    "friends": [{"id": 63, "name": "Derek Greene"},
                {"id": 68, "name": "Renee Doyle"},
                {"id": 36, "name": "James Livingston"},
                {"id": 43, "name": "Tyler Murphy"}],
    "gender": "female",
    "greeting": "Howdy!",
    "guid": "749a6412-63a0-47f8-bdd8-268bd5c2162b",
    "index": 2566,
    "isActive": False,
    "latitude": 36.215487,
    "longitude": 71.038439,
    "name": "Lisa Henson",
    "phone": "791.126.7836x392",
    "picture": "https://somehost.io/37x29",
    # "registered": str(datetime.datetime(2019, 11, 16, 19, 18, 24, 256)),
    "tags": ["data science",
             "internet",
             "california",
             "bike",
             "lakers",
             "food",
             "travelling",
             "hiking"]
}


class ItemServiceServicer(item_pb2_grpc.ItemServiceServicer):
    def string(self, request, context):
        result = item_pb2.ItemResponse(message="grpc answer")
        return result

    # def orjson(self, request, context):
    #     title = request.title
    #     result = item_pb2.ItemResponse(message=orjson.dumps({"who?": "grpc"}))
    #     # print('result =', result)
    #     return result

    def dictionary(self, request, context):
        # title = request.title
        # result = item_pb2.ItemResponse(message=json.dumps({"who?": "grpc"}))
        data = deepcopy(TEST_DATA)
        for k, v in data.items():
            if isinstance(v, str):
                data[k] = data[k] + ''.join([str(v) for v in range(10)])
        result = item_pb2.ItemResponse(message=json.dumps(data))
        return result


_ONE_DAY_IN_SECONDS = 60 * 60 * 24


def serve(port):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    item_pb2_grpc.add_ItemServiceServicer_to_server(ItemServiceServicer(), server)
    server.add_insecure_port(f"[::]:{port}")
    server.start()
    print(f"gRPC server started on port {port}")
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == "__main__":
    """
    python server.py --port 7102
    """
    values = sys.argv
    port = int(values[values.index("--port") + 1])
    serve(port)
