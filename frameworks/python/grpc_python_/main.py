from fast_grpc import FastGRPC, FastGRPCService, grpc_method
from pydantic import BaseModel


class ItemRequest(BaseModel):
    title: str


class ItemResponse(BaseModel):
    title: str


class MotherFucker(FastGRPCService):
    @grpc_method(request_model=ItemRequest, response_model=ItemResponse)
    async def get_itme(self, request: ItemRequest) -> ItemResponse:
        return ItemResponse(text=f"Hello, {request.name}!")


app = FastGRPC(MotherFucker())
app.run()
