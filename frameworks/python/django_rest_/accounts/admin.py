from django.contrib import admin
from django.contrib.auth import get_user_model


User = get_user_model()


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    class Meta:
        model = User

    search_fields = ["id", "username", "email"]
    list_display = ["id", "username", "email"]
    list_display_links = ["id", "username"]

    list_filter = []
