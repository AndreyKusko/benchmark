from django.contrib.auth import get_user_model
from rest_framework import serializers

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    """Serialize User."""

    email = serializers.EmailField()

    class Meta:
        model = User
        fields = ["__all__"]

    def update(self, instance, validated_data):
        if password := validated_data.pop("password", None):
            instance.set_password(password)
        return super().change(instance, validated_data)
