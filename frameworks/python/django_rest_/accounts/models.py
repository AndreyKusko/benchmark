from django.contrib.auth.models import AbstractUser, UserManager
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager

NOT_TA_REQUESTING_USER_REASON = "Запрашивающий пользователь не активен. Причина: {reason}"


class User(AbstractUser):
    username_validator = UnicodeUsernameValidator()

    telegram_username = models.CharField(max_length=512,null=True,  blank=True)
    telegram_user_id = models.CharField(max_length=512, null=True, blank=True)
    username = models.CharField(
        _("username"),
        max_length=150,
        unique=True,
        help_text=_(
            "Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only."
        ),
        null=True, blank=True,  # для телеграмных юзеров
        validators=[username_validator],
        error_messages={
            "unique": _("A user with that username already exists."),
        },

    )
    email = models.EmailField(unique=True, null=True, blank=True)
    is_verified_email = models.BooleanField(default=False)

    is_blocked = models.BooleanField(default=False, help_text="Бан от админов")
    blocking_reason = models.CharField(
        max_length=256,
        blank=True,
        null=True,
        help_text="Причина, по которой заблокировали пользователя",
    )

    groups = models.ManyToManyField(
        blank=True,
        help_text="The groups this user belongs to. A user will get all permissions granted to each of their groups.",
        related_name="user_set",
        related_query_name="user",
        to="auth.Group",
        verbose_name="groups",
    )
    user_permissions = models.ManyToManyField(
        blank=True,
        help_text="Specific permissions for this user.",
        related_name="user_set",
        related_query_name="user",
        to="auth.Permission",
        verbose_name="user permissions",
    )

    class Meta:
        ordering = ("-id",)
        db_table = "user"
        verbose_name_plural = "Users"

    def __str__(self) -> str:
        return f"{self.id} | {self.username}"
