from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from rest_framework.permissions import AllowAny
from rest_framework.routers import DefaultRouter
from rest_framework.schemas import get_schema_view

from main.views import s_response, response_dictionary, d_n_s_response, dictionary


schema_view = get_schema_view(
    title="Benchmark django_rest_",
    version="v1",
    public=True,
    permission_classes=(AllowAny,),
    url=settings.HOST,
)


urlpatterns = [
    path("admin/", admin.site.urls),
    path("dictionary", dictionary, name="json_"),
    path("response-dictionary", response_dictionary, name="j_response"),
    path("serialize", s_response, name="s_response"),
    path("deserialize-n-serialize", d_n_s_response, name="d_n_s_response"),

]

if settings.DEBUG:
    # urlpatterns += [
    #     url(r"^silk/", include("silk.urls", namespace="silk"))
    # ]
    # import debug_toolbar
    # urlpatterns += [path("__debug__/", include(debug_toolbar.urls))]
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
