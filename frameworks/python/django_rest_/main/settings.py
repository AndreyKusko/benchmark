import os
from pathlib import Path
from typing import Optional
from urllib.parse import urlparse

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'main.settings')

BASE_DIR = Path(__file__).resolve().parent.parent
# BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = "django-sdcocrieco-skjdbfi97sewirg_239809r12jwqpidnojfvudfgr@123"

DEBUG = True

ALLOWED_HOSTS = [
    "*",
]
CSRF_TRUSTED_ORIGINS = [
    "http://127.0.0.1",
]

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "rest_framework",
    "accounts",
]
MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]


ROOT_URLCONF = "main.urls"
WSGI_APPLICATION = "main.wsgi.application"


# TODO remove
class ParseURI:
    options: Optional[dict] = None
    name: Optional[str] = None
    user: Optional[str] = None
    password: Optional[str] = None
    hostname: Optional[str] = None
    post: Optional[str] = None
    port: Optional[str] = None

    def __init__(self, uri) -> None:
        result = urlparse(uri)
        raw_options = [d.split("=") for d in result.query.split("&") if d]
        self.options = dict(raw_options) if raw_options else {}
        self.name = result.path[1:]
        self.user = result.username
        self.password = result.password
        self.host = result.hostname
        self.port = result.port
        self.port = result.port


__default_db_uri = "postgresql://postgres:postgres@localhost:5432/benchmark_django_rest?sslmode=disable&sslrootcert="
DATABASE_URI = os.environ.get("DATABASE_URI_KABA", __default_db_uri)
db = ParseURI(DATABASE_URI)

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": db.name,
        "USER": db.user,
        "PASSWORD": db.password,
        "HOST": db.hostname,
        "PORT": db.port,
        "OPTIONS": db.options,
    },
}
AUTH_PASSWORD_VALIDATORS = [
    {"NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"},
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]
LANGUAGE_CODE = "en-us"
TIME_ZONE = "UTC"
USE_I18N = True
USE_TZ = True
# STATIC_URL = '/opt/kaba/static/'
# STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), "static")
DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

HOST = os.environ.get("BASE_URL", "http://localhost")
API_V1 = "api/v1"

AUTH_USER_MODEL = "accounts.User"

DEFAULT_AUTHENTICATION_CLASSES = "DEFAULT_AUTHENTICATION_CLASSES"
DEFAULT_RENDERER_CLASSES = "DEFAULT_RENDERER_CLASSES"

REST_FRAMEWORK = {
    # DEFAULT_AUTHENTICATION_CLASSES: ["ma_saas.auth.TokenAuthentication"],
    DEFAULT_RENDERER_CLASSES: ["main.json_renderer.UTF8CharsetJSONRenderer"],
    "DEFAULT_PERMISSION_CLASSES": [
        "rest_framework.permissions.AllowAny",
        # "rest_framework.permissions.DjangoModelPermissions",
    ],
    "DEFAULT_PAGINATION_CLASS": "rest_framework.pagination.LimitOffsetPagination",
    # "DEFAULT_THROTTLE_RATES": {"anon_geocoder": "10/minute"},
}

if DEBUG:
    REST_FRAMEWORK[DEFAULT_RENDERER_CLASSES].insert(0, "rest_framework.renderers.BrowsableAPIRenderer")
    SWAGGER_SETTINGS = {
        "SECURITY_DEFINITIONS": {"DRF Token": {"type": "apiKey", "name": "Authorization", "in": "header"}},
    }

STATIC_URL = "/static/"
STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), "static")
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
    # os.path.join(BASE_DIR, 'src'),
)

MEDIA_URL = "/media/"
MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), "media")

SUPERUSER = os.environ.get("SUPERUSER", "alpha_alpha@is.me_123qweasd")

DEFAULT_IP = "http://127.0.0.1:28001"


TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
        ],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
            "builtins": [
            ],
            # or as @x-yuri pointed out, you can put them in `libraries`
            "libraries": {
            },
        },
    },
]