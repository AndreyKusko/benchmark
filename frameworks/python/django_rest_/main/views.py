import json

from django.shortcuts import render
from rest_framework.response import Response

from rest_framework.decorators import api_view

from main.serializers import ItemSerializer


TEST_DATA = {
    "_id": "801512f3ff9658d50e47fe90",
    "about": "I’m currently a Senior Data Science Manager at Indeed.com, where I help our Job Search Front End, Search Matching/Ranking, and Taxonomy teams.",
    "address": "087 Simmons Greens Apt. 948\nGinastad, LA 61807",
    "age": 32,
    "balance": "$5,583.41",
    # "birthday": str(datetime.datetime(1990, 5, 4, 0, 0, 0, 0)),
    "company": "Indeed.com",
    "email": "lisahenson@indeed.com",
    "eyeColor": "#e2b21f",
    "favorite": "whole",
    "friends": [{"id": 63, "name": "Derek Greene"},
                {"id": 68, "name": "Renee Doyle"},
                {"id": 36, "name": "James Livingston"},
                {"id": 43, "name": "Tyler Murphy"}],
    "gender": "female",
    "greeting": "Howdy!",
    "guid": "749a6412-63a0-47f8-bdd8-268bd5c2162b",
    "index": 2566,
    "isActive": False,
    "latitude": 36.215487,
    "longitude": 71.038439,
    "name": "Lisa Henson",
    "phone": "791.126.7836x392",
    "picture": "https://somehost.io/37x29",
    # "registered": str(datetime.datetime(2019, 11, 16, 19, 18, 24, 256)),
    "tags": ["data science",
             "internet",
             "california",
             "bike",
             "lakers",
             "food",
             "travelling",
             "hiking"]
}

@api_view(['GET',])
def dictionary(request) -> Response:
    """смотреть на этот модуль нет смысла"""
    return Response(data=TEST_DATA)

@api_view(['GET',])
def response_dictionary(request) -> Response:
    return Response(data=TEST_DATA)


@api_view(['GET',], )
def s_response(request, ):
    serializer = ItemSerializer(data=TEST_DATA)
    serializer.is_valid(raise_exception=True)
    return Response(data=serializer.data)


@api_view(['POST',])
def d_n_s_response(request) -> Response:
    serializer = ItemSerializer(data=json.loads(request.body))
    serializer.is_valid(raise_exception=True)
    return Response(data=serializer.data)

