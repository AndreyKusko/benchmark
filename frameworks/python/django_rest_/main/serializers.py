from rest_framework import serializers


class ItemSerializer(serializers.Serializer):
    _id = serializers.CharField(max_length=25)
    about = serializers.CharField(max_length=150)
    address = serializers.CharField(max_length=150)
    age = serializers.IntegerField()
    balance = serializers.CharField(max_length=25)
    company = serializers.CharField(max_length=25)
    email = serializers.CharField(max_length=25)
    eyeColor = serializers.CharField(max_length=25)
    favorite = serializers.CharField(max_length=25)
    friends = serializers.ListSerializer(child=serializers.DictField())
    gender = serializers.CharField(max_length=10)
    greeting = serializers.CharField(max_length=10)
    guid = serializers.CharField(max_length=40)
    index = serializers.IntegerField()
    isActive = serializers.BooleanField()
    latitude = serializers.FloatField()
    longitude = serializers.FloatField()
    name = serializers.CharField(max_length=20)
    phone = serializers.CharField(max_length=20)
    picture = serializers.CharField(max_length=40)
    tags = serializers.ListSerializer(child=serializers.CharField(max_length=25))

    class Meta:
        fields = [
            '_id', 'about', 'address', 'age', 'balance',
            'company', 'email', 'eyeColor', 'favorite', 'friends',
            'gender', 'greeting', 'guid', 'index', 'isActive',
            'latitude', 'longitude', 'name', 'phone', 'picture', 'tags'
        ]