#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""

import os
import posixpath
import sys

from main.settings import DEFAULT_IP


def main() -> None:
    """Run administrative tasks."""
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "main.settings")
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        msg = (
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        )
        raise ImportError(
            msg,
        ) from exc
    # posixpath.join(os.environ.get("SERVER_IP", DEFAULT_IP), "admin", "")
    execute_from_command_line(sys.argv)
    # execute_from_command_line(sys.argv + ['--noreload'])


if __name__ == "__main__":
    main()

