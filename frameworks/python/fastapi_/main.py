from contextlib import contextmanager
from copy import deepcopy

from fastapi.responses import JSONResponse, RedirectResponse
from fastapi import FastAPI, Response
from pydantic import BaseModel
from fastapi import status


app = FastAPI()


class Item(BaseModel):
    _id: str
    about: str
    address: str
    age: int
    balance: str
    company: str
    email: str
    eyeColor: str
    favorite: str
    friends: list
    gender: str
    greeting: str
    guid: str
    index: int
    isActive: bool
    latitude: float
    longitude: float
    name: str
    phone: str
    picture: str
    tags: list


TEST_DATA = {
    "_id": "801512f3ff9658d50e47fe90",
    "about": "I’m currently a Senior Data Science Manager at Indeed.com, where I help our Job Search Front End, Search Matching/Ranking, and Taxonomy teams.",
    "address": "087 Simmons Greens Apt. 948\nGinastad, LA 61807",
    "age": 32,
    "balance": "$5,583.41",
    # "birthday": str(datetime.datetime(1990, 5, 4, 0, 0, 0, 0)),
    "company": "Indeed.com",
    "email": "lisahenson@indeed.com",
    "eyeColor": "#e2b21f",
    "favorite": "whole",
    "friends": [{"id": 63, "name": "Derek Greene"},
                {"id": 68, "name": "Renee Doyle"},
                {"id": 36, "name": "James Livingston"},
                {"id": 43, "name": "Tyler Murphy"}],
    "gender": "female",
    "greeting": "Howdy!",
    "guid": "749a6412-63a0-47f8-bdd8-268bd5c2162b",
    "index": 2566,
    "isActive": False,
    "latitude": 36.215487,
    "longitude": 71.038439,
    "name": "Lisa Henson",
    "phone": "791.126.7836x392",
    "picture": "https://somehost.io/37x29",
    # "registered": str(datetime.datetime(2019, 11, 16, 19, 18, 24, 256)),
    "tags": ["data science",
             "internet",
             "california",
             "bike",
             "lakers",
             "food",
             "travelling",
             "hiking"]
}

serializer = Item(**TEST_DATA)


@app.get("/dictionary")
def dictionary():
    return TEST_DATA


@app.get("/response-dictionary")
def response_dictionary() -> Response:
    return JSONResponse(content=TEST_DATA)


@app.get("/serialize", status_code=status.HTTP_200_OK, response_model=Item)
def serialize() -> Response:
    return serializer


@app.post("/deserialize-n-serialize", status_code=status.HTTP_200_OK, response_model=Item)
def deserialize_n_serialize(item: Item) -> Response:
    # item.validate()
    # data = item.json
    # for k,v in data.items():
    #     if isinstance(v, str):
    #         data[k] = data[k]+''.join([str(v) for v in range(10)])
    return Item(**TEST_DATA)

