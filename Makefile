# source /opt/benchmark/caller/venv/bin/activate
ACTIVATE_CALLER=/opt/benchmark/caller/venv/bin/activate
# source /opt/benchmark/litestar/venv/bin/activate
ACTIVATE_LITESTAR_PYTHON=/opt/benchmark/litestar/venv/bin/activate
# source /opt/benchmark/django_rest/venv/bin/activate
# source /opt/benchmark/fastapi/venv/bin/activate
ACTIVATE_FASTAPI_PYTHON=/opt/benchmark/fastapi/venv/bin/activate
# source /opt/benchmark/fastapi/venv/bin/activate
ACTIVATE_FASTAPI_PYPY=/opt/benchmark/fastapi/venvpypy/bin/activate
# source /opt/benchmark/blacksheep/venv/bin/activate
ACTIVATE_BLACKSHEEP_PYTHON=/opt/benchmark/blacksheep/venv/bin/activate
# source /opt/benchmark/aiohttp/venv/bin/activate
ACTIVATE_AIOHTTP_PYTHON=/opt/benchmark/aiohttp/venv/bin/activate
# source /opt/benchmark/grpc_python/venv/bin/activate
ACTIVATE_GRPC_PYTHON=/opt/benchmark/grpc_python/venv/bin/activate
# source /opt/benchmark/grpc_python/venvpypy/bin/activate
ACTIVATE_GRPC_PYPY=/opt/benchmark/grpc_python/venvpypy/bin/activate
# source /opt/benchmark/sanic/venv/bin/activate
ACTIVATE_SANIC_PYTHON=/opt/benchmark/sanic/venv/bin/activate



BENCHMARK_PATH=/opt/benchmark
#/opt/benchmark/frameworks/django_rest
FRAMEWORKS_PYTHON=frameworks/python

# django rest
DJANGO_REST = django_rest_
DJANGO_REST_PYTHON_PATH = $(BENCHMARK_PATH)/$(FRAMEWORKS_PYTHON)/$(DJANGO_REST)
DJANGO_REST_PYTHON_VENV_PATH = $(DJANGO_REST_PYTHON_PATH)/venv
ACTIVATE_DJANGO_REST_PYTHON=$(DJANGO_REST_PYTHON_VENV_PATH)/bin/activate
DJANGO_REST_PROJECT_PATH=$(FRAMEWORKS_PYTHON)/$(DJANGO_REST)

ORM_PYTHON:=orm/python/
ORM_PYTHON_DJANGOORM=/opt/benchmark/orm/python/djangoorm_/venv/bin/activate
DJANGOORM_SYNC_REPO=$(ORM_PYTHON)djangoorm_
ORM_PYTHON_SQLALCHEMY=/opt/benchmark/orm/python/sqlalchemy_/venv/bin/activate
SQLALCHEMY_SYNC_REPO=$(ORM_PYTHON)sqlalchemy_


#benchmark_dir:
#	mkdir -p /opt
#	mkdir -p /opt/benchmark
#	mkdir -p /opt/benchmark/orm
#	mkdir -p /opt/benchmark/framework

prepare_orm_python_sqlalchemy:
	mkdir -p /opt/benchmark/orm/python/sqlalchemy_
	python3.10 -m venv /opt/benchmark/orm/python/sqlalchemy_/venv
	. $(ORM_PYTHON_SQLALCHEMY); pip install --upgrade pip
	. $(ORM_PYTHON_SQLALCHEMY); pip install -r $(SQLALCHEMY_SYNC_REPO)/requirements.txt
prepare_orm_python_djangoorm:
	mkdir -p /opt/benchmark/orm/python/djangoorm_
	python3.10 -m venv /opt/benchmark/orm/python/djangoorm_/venv
	. $(ORM_PYTHON_DJANGOORM); pip install --upgrade pip;
	. $(ORM_PYTHON_DJANGOORM); pip install -r $(DJANGOORM_SYNC_REPO)/requirements.txt;

prepare_caller:
	mkdir -p /opt/benchmark/caller
	python3.10 -m venv /opt/benchmark/caller/venv
	. $(ACTIVATE_CALLER); pip install --upgrade pip
	. $(ACTIVATE_CALLER); pip install -r caller_/requirements.txt

prepare_fastapi_python:
	mkdir -p /opt/benchmark/fastapi
	python3.10 -m venv /opt/benchmark/fastapi/venv
	. $(ACTIVATE_FASTAPI_PYTHON); pip install --upgrade pip
	. $(ACTIVATE_FASTAPI_PYTHON); pip install -r $(FRAMEWORKS_PYTHON)/fastapi_/requirements.txt
prepare_fastapi_pypy:
	mkdir -p /opt/benchmark/fastapi
	pypy3 -m venv /opt/benchmark/fastapi/venvpypy
	. $(ACTIVATE_FASTAPI_PYPY); pip install --upgrade pip
	. $(ACTIVATE_FASTAPI_PYPY); pip install -r $(FRAMEWORKS_PYTHON)/fastapi_/requirements.txt



path_django_rest_python:
	echo "source $(ACTIVATE_DJANGO_REST_PYTHON)"
	echo "cd $(DJANGO_REST_PROJECT_PATH)"
prepare_django_rest_python:
	mkdir -p $(DJANGO_REST_PYTHON_PATH)
	python3.10 -m venv $(DJANGO_REST_PYTHON_VENV_PATH)
	. $(ACTIVATE_DJANGO_REST_PYTHON); pip install --upgrade pip
	. $(ACTIVATE_DJANGO_REST_PYTHON); pip install -r $(DJANGO_REST_PROJECT_PATH)/requirements.txt
django_rest_python:
	make prepare_django_rest_python
	echo ACTIVATE_DJANGO_REST_PYTHON
	. $(ACTIVATE_DJANGO_REST_PYTHON); uvicorn --app-dir $(DJANGO_REST_PROJECT_PATH) main.asgi:application --reload --port 8301


prepare_litestar_python:
	mkdir -p /opt/benchmark/litestar
	python3.10 -m venv /opt/benchmark/litestar/venv
	. $(ACTIVATE_LITESTAR_PYTHON);pip install --upgrade pip
	. $(ACTIVATE_LITESTAR_PYTHON); pip install -r $(FRAMEWORKS_PYTHON)/litestar_/requirements.txt
prepare_blacksheep_python:
	mkdir -p /opt/benchmark
	mkdir -p /opt/benchmark/blacksheep
	python3.10 -m venv /opt/benchmark/blacksheep/venv
	. $(ACTIVATE_BLACKSHEEP_PYTHON); pip install --upgrade pip
	. $(ACTIVATE_BLACKSHEEP_PYTHON); pip install -r $(FRAMEWORKS_PYTHON)/blacksheep_/requirements.txt
prepare_aiohttp_python:
	mkdir -p /opt/benchmark
	mkdir -p /opt/benchmark/aiohttp
	python3.10 -m venv /opt/benchmark/aiohttp/venv
prepare_sanic_python:
	mkdir -p /opt/benchmark
	mkdir -p /opt/benchmark/sanic
	python3.10 -m venv /opt/benchmark/sanic/venv
prepare_grpc_python:
	mkdir -p /opt/benchmark
	mkdir -p /opt/benchmark/grpc_python
	python3.10 -m venv /opt/benchmark/grpc_python/venv
prepare_grpc_pypy:
	mkdir -p /opt/benchmark
	mkdir -p /opt/benchmark/grpc_python
	pypy3 -m venv /opt/benchmark/grpc_python/venvpypy

meow:
	echo $(MESSAGE1)
	#. $(ACTIVATE_CALLER); python3 caller_/main.py
caller:
	# QTY=10 ORM=true HTTP=true GRPC=true make caller
	# QTY=10 ORM=true make caller
	#make prepare_caller
	. $(ACTIVATE_CALLER); python3 caller_/main.py qty=$(QTY) orm=$(ORM) grpc=$(GRPC) http=$(HTTP)
fastapi_python:
	make prepare_fastapi_python
	. $(ACTIVATE_FASTAPI_PYTHON); uvicorn --app-dir $(FRAMEWORKS_PYTHON)/fastapi_ main:app --reload --port 8101
fastapi_pypy:
	make prepare_fastapi_pypy
	. $(ACTIVATE_FASTAPI_PYPY); uvicorn --app-dir $(FRAMEWORKS_PYTHON)/fastapi_ main:app --reload --port 8102
litestar_python:
	make prepare_litestar_python
	. $(ACTIVATE_LITESTAR_PYTHON); uvicorn --app-dir $(FRAMEWORKS_PYTHON)/litestar_ main:app --reload --port 8201
blacksheep_python:
	make prepare_blacksheep_python
	. $(ACTIVATE_BLACKSHEEP_PYTHON); uvicorn --app-dir $(FRAMEWORKS_PYTHON)/blacksheep_ main:app --reload --port 8401
#aiohttp_python:
#	PATH=$(FRAMEWORKS_PYTHON)aiohttp_/
#	. $(ACTIVATE_AIOHTTP_PYTHON); pip install --upgrade pip
#	. $(ACTIVATE_AIOHTTP_PYTHON); pip install -r $(PATH)requirements.txt
#	. $(ACTIVATE_AIOHTTP_PYTHON); python3 $(PATH)main.py --port 8501
#sanic_python:
#	. $(ACTIVATE_SANIC_PYTHON); pip install --upgrade pip
#	. $(ACTIVATE_SANIC_PYTHON); pip install -r $(FRAMEWORKS_PYTHON)sanic_/requirements.txt
#	. $(ACTIVATE_SANIC_PYTHON); python3 $(FRAMEWORKS_PYTHON)sanic_/main.py --port 8601


# ORM
orm_python_djangoorm:
	make prepare_orm_python_djangoorm
	. $(ORM_PYTHON_DJANGOORM); uvicorn --app-dir $(DJANGOORM_SYNC_REPO) main:app --reload --port 6114;
orm_python_sqlalchemy_async_session:
	make prepare_orm_python_sqlalchemy
	. $(ORM_PYTHON_SQLALCHEMY); uvicorn --app-dir $(SQLALCHEMY_SYNC_REPO) main_async_session:app --port 6112
orm_python_sqlalchemy_sync_repo:
	make prepare_orm_python_sqlalchemy
	. $(ORM_PYTHON_SQLALCHEMY); uvicorn --app-dir $(SQLALCHEMY_SYNC_REPO) main_sync_repo:app --port 6116
orm_python_sqlalchemy_async_repo:
	make prepare_orm_python_sqlalchemy
	. $(ORM_PYTHON_SQLALCHEMY); uvicorn --app-dir $(SQLALCHEMY_SYNC_REPO) main_async_repo:app --port 6115


grpc_python: . $(ACTIVATE_GRPC_PYTHON);
	pip install --upgrade pip
	pip install -r grpc_python_/requirements.txt
	python3.10 grpc_python_/server.py --port 7101
grpc_pypy: . $(ACTIVATE_GRPC_PYPY);
	pip install --upgrade pip
	pip install -r grpc_python_/requirements_pypy.txt
	python3.10 grpc_python_/server_pypy.py --port 7102
